<div class="navbar navbar-fixed-top navbar-inverse">
	<div class="navbar-inner">
		<div id="menu">
			<ul class="nav">
				<li class="<?php if (current_url() == site_url()){ echo "active"; }  ?> divider-vertical"><a href="<?php echo base_url(); ?>">INICIO</a></li>
				<li class="<?php if (current_url() == site_url("revenges/score")){ echo "active"; }  ?> divider-vertical"><a href="<?php echo site_url("revenges/score"); ?>">MAS VOTADAS</a></li>
				<li class="<?php if (current_url() == site_url("revenges/create")){ echo "active"; }  ?> divider-vertical"><a href="<?php echo site_url("revenges/create"); ?>">VENGATE YA</a></li>
				<form class="navbar-search">
					<input type="text" class="search-query" placeholder="Nombre y Apellido">
					<button type="submit" class="btn btn-danger"><div class="icon-search"></div></button>
					
				</form>
			</ul>
		</div>

	</div>
</div>