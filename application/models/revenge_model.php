<?php
class Revenge_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_revenges()
	{
		$query = $this->db->get('revenges');
		return $query->result_array();
	}
	
	public function get_revenges_id_desc()
	{
		$this->db->order_by("id", "desc"); 
		$query = $this->db->get('revenges');
		return $query->result_array();
	}
	
	public function get_revenges_score_desc()
	{
		$this->db->order_by("score", "desc"); 
		$query = $this->db->get('revenges');
		return $query->result_array();
	}
	
	public function get_revenges_quantity(){
		return $this->db->count_all('revenges');
	}
	
	public function increase_score($id){
		$this->db->where('id', $id);
		$this->db->set('score', 'score+1', FALSE);
		return $this->db->update('revenges');
	}
	
	public function decrease_score($id){
		$this->db->where('id', $id);
		$this->db->set('score', 'score-1', FALSE);
		return $this->db->update('revenges');
	}
	
	public function set_revenge()
	{
		
		$data = array(
			'name' => $this->input->post('name'),
			'description' => $this->input->post('description')
		);
		
		return $this->db->insert('revenges', $data);
	}
}