<?php

class Pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('revenge_model');
		$this->load->helper('url');
	}

	public function view($page = 'home')
	{
		if ( ! file_exists('application/views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		
		$data['title'] = ucfirst($page); // Capitalize the first letter
		
		if( $page == 'home' ){
			$data['revenges'] = $this->revenge_model->get_revenges_id_desc();
			//$data['revenges_quantity'] = $this->revenge_model->get_revenges_quantity();
		}
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu');
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer');
	}
}
