<?php
class Revenges extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('revenge_model');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['revenges'] = $this->revenge_model->get_revenges();
		
		$data['title'] = 'News archive';

		$this->load->view('templates/header', $data);
		$this->load->view('news/index', $data);
		$this->load->view('templates/footer');
	}
	
	public function score()
	{
		$data['revenges'] = $this->revenge_model->get_revenges_score_desc();
		
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu');
		$this->load->view('pages/home', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug)
	{
		//$data['news_item'] = $this->revenge_model->get_news($slug);

		if (empty($data['news_item']))
		{
			show_404();
		}

		$data['title'] = $data['news_item']['title'];

		$this->load->view('templates/header', $data);
		$this->load->view('news/view', $data);
		$this->load->view('templates/footer');
	}
	
	public function increaseScore()
	{
		$this->revenge_model->increase_score($this->input->post('revengeId'));
	}
	
	public function decreaseScore()
	{
		$this->revenge_model->decrease_score($this->input->post('revengeId'));
	}
	
	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = 'Create a news item';
		
		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('description', 'Descripcion', 'required');
		
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('templates/menu');			
			$this->load->view('revenges/create');
			$this->load->view('templates/footer');
			
		}
		else
		{
			$this->revenge_model->set_revenge();
			$this->load->view('templates/header');
			$this->load->view('templates/menu');			
			$this->load->view('revenges/success');
			$this->load->view('templates/footer');
		}
	}
	
}